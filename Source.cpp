#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <list>
#include <vector>
#include <algorithm>

using namespace std;

class Item
{
public:
    string* name;

    void deepCopy(const Item& cloneItem) {

        int length = strlen(cloneItem.name->c_str());
        delete[] name;
        if (cloneItem.name)
        {
            name = new string;

            for (int i = 0; i < length; ++i)
                name[i] = cloneItem.name[i];
        }
        else
            name = nullptr;
    }

    Item()
    {
        name = (string*)"None";
    }

    Item(const char* newName)
    {
        name = (string*)newName;
    }

    Item(const Item& source)
    {
        deepCopy(source);
    }

    ~Item()
    {
        delete[] name;
    }

    Item operator=(const Item& source)
    {
        if (this != &source)
        {
            deepCopy(source);
        }
        return *this;
    }

    
};

int main() 
{
    //copy
    vector<Item*> invToCopy;
    invToCopy.push_back(new Item("Sword"));
    invToCopy.push_back(new Item("Axe"));
    invToCopy.push_back(new Item("Pickaxe"));
    vector<Item*> invCopy;
    invCopy.resize(invToCopy.size());
    copy(invToCopy.begin(), invToCopy.end(), invCopy.begin());
    //find
    vector<int> intVector;
    intVector.resize(10);
    srand(time(NULL));
    fill(intVector.begin(), intVector.end(), rand()%10);
}
